#include "stm8s.h"
#include "custom.h"

uint8_t flag=0;
uint32_t cas;       //Globální veličiny

uint32_t vterina;
uint32_t sekund;
uint32_t minuta;
uint32_t minut;
uint32_t alpha;

void zapsat(void)
{
    GPIO_Write(GPIOG,vterina);
    GPIO_Write(GPIOD,sekund);
    GPIO_Write(GPIOA,minuta);
    GPIO_Write(GPIOB,minut*4);
}

void prepocet(void)
{
    sekund = cas % 60;
    vterina = sekund % 10;
    sekund = sekund - vterina;
    sekund = sekund / 10;
    alpha = cas - vterina - 10*sekund;
    alpha = alpha / 60;
    minuta = alpha % 10;
    alpha = alpha - minuta;
    minut = alpha/10; 
    if(minut == 10)
    {
        cas == 0;
        prepocet();
    }
    zapsat();
}

INTERRUPT_HANDLER(EXTI_PORTE_IRQHandler, 7)
{
    
    flag = flag + 1;
    if (flag == 3)
        {flag = 0;}
    for(uint32_t i ; i < 1000 ; i++)
        {
        ;
        }
}

void main(void)
{
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // FREQ MCU 16MHz

    GPIO_Init(GPIOD, GPIO_PIN_0, GPIO_MODE_OUT_PP_LOW_SLOW); //PORT NA JEDNOTKY SEKUND
    GPIO_Init(GPIOD, GPIO_PIN_1, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOD, GPIO_PIN_2, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOD, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);

    GPIO_Init(GPIOG, GPIO_PIN_0, GPIO_MODE_OUT_PP_LOW_SLOW); //PORT NA DESÍTKY SEKUND
    GPIO_Init(GPIOG, GPIO_PIN_1, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOG, GPIO_PIN_2, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOG, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);

    GPIO_Init(GPIOA, GPIO_PIN_1, GPIO_MODE_OUT_PP_LOW_SLOW); //PORT NA JEDNOTKY MINUT
    GPIO_Init(GPIOA, GPIO_PIN_2, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOA, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOA, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_SLOW);

    GPIO_Init(GPIOF, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW); //PORT NA DESÍTKY MINUT
    GPIO_Init(GPIOF, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOF, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOF, GPIO_PIN_6, GPIO_MODE_OUT_PP_LOW_SLOW);

    GPIO_Init(GPIOE, GPIO_PIN_4, GPIO_MODE_IN_FL_IT);

    GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);

    EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOE, EXTI_SENSITIVITY_RISE_ONLY);
    ITC_SetSoftwarePriority(ITC_IRQ_PORTE, ITC_PRIORITYLEVEL_0);
    enableInterrupts();
    
    delay_s_init();

    
    while (1)
    {   if(flag == 0)
        {
            delay_s(1);
            cas = cas + 1;
            GPIO_WriteReverse(GPIOC, GPIO_PIN_5);
            prepocet();
        }
        else if(flag == 1)
        {
            ;
        }
        else if(flag == 2)
        {
            cas = 0;
            prepocet();
            TIM3_ClearFlag (TIM3_FLAG_UPDATE);
        } 
        
    
            
           
            
    }
}
