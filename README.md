# Stopky


## Úvod

Stopky, navržené na nepájivém poli s pomocí STM8 Nucleo. Závěrečná práce do předmětu MIT.

## Seznam součástek

| Typ součastky | Počet kusů |  Cena za kus |  Cena dohromady |
|:-------------:|:----------:|:------------:|:---------------:|
| STM8 Nucleo   |      1     |    250Kč     |      250Kč      |
| 7seg. displej |      4     |      7Kč     |       28Kč      |
| Dekodér D147D |      4     |     24Kč     |       96Kč      |
|    Rezistor   |     29     |      2Kč     |       58Kč      |
|     Káblíky   |     50     |      1Kč     |       50Kč      |
|    Celkem     |            |              |      482Kč      |

## Blokové schéma
```mermaid
flowchart LR
    PC-->USB
    USB-->STM8
subgraph Dekodery
    STM8-->1.Dekoder
    STM8-->2.Dekoder
    STM8-->3.Dekoder
    STM8-->4.Dekoder
    end
subgraph Sedmisegmenty
    1.Dekoder-->1.Sedmisegment
    2.Dekoder-->2.Sedmisegment
    3.Dekoder-->3.Sedmisegment
    4.Dekoder-->4.Sedmisegment
    end
```
## Schéma v Kicadu
![Schéma](schema_stopky.jpg)

## Zapojení
![Zapojení](zapojeni.jpg)

## Zdrojový kód
Zdrojový kód je součástí přílohy

## Závěr
Jelikož programování jako takové mi vůbec nejde tak pro mě byl projekt velice náročný a při realizaci jsem narážel na spoutu náročných věcí. Bohužel mi projekt nevyšel vůbec podle mých představ. 

## Autor
Rozložník Radek
